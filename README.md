# Guess the number

### Setup tutorial

* Make sure javac and java executables are in your PATH.
* Go to "GuessTheNumber" folder in cmd.
* "javac GuessTheNumberServer/src/main/java/guessTheNumberServer/*.java"
* "javac GuessTheNumberClient/src/main/java/guessTheNumberClient/*.java"

#### Run server

* In cmd change folder to "GuessTheNumberServer/src/main/java/"
* Run server with "java guessTheNumberServer.Server"
* If the server doesn't start, try changing SERVER_PORT variable.

#### Run client

* In cmd change folder to "GuessTheNumberClient/src/main/java/"
* Run server with "java guessTheNumberClient.Client"

### Game tutorial

* Run the server.
* Run the client.
* Input a number in the client's console.
* If you guess is incorrect, the game asks to guess again.
* The game remembers how many incorrect guesses you have.
* If your guess is correct, the game asks if you want to play again or quit. To try again input "y" or "Y". To quit the game input "n" or "N".