package guessTheNumberServer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PlayStateTest {
    private Game game;
    private State state;

    @Before
    public void setupEnvironment() {
        game = new Game();
        state = new PlayState();
        game.processInput("initial call");
    }

    @Test
    public void shouldAskForANumberWhenInputtingGuesses() {
        assertEquals(state.generateOutput(game, "asd"), "Please enter a number.");
    }

    @Test
    public void shouldIncrementGuessCountAndSpecifyToGuessHigher() {
        int answer = game.getAnswer();
        int guessCount = game.getGuessCount();
        String output = state.generateOutput(game, String.format("%d", answer - 1));
        assertEquals(game.getGuessCount(), guessCount + 1);
        assertEquals(output, "Incorrect. Try a bigger number.");
    }

    @Test
    public void shouldIncrementGuessCountAndSpecifyToGuessLower() {
        int answer = game.getAnswer();
        int guessCount = game.getGuessCount();
        String output = state.generateOutput(game, String.format("%d", answer + 1));
        assertEquals(game.getGuessCount(), guessCount + 1);
        assertEquals(output, "Incorrect. Try a smaller number.");
    }

    @Test
    public void shouldChangeGameStateFromPlayToEndAfterCorrectGuess() {
        state.generateOutput(game, String.format("%d", game.getAnswer()));
        assertTrue(game.getState() instanceof EndState);
    }
}
