package guessTheNumberServer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class StartStateTest {
    private Game game;
    private State state;

    @Before
    public void setupEnvironment() {
        game = new Game();
        state = new StartState();
    }

    @Test
    public void shouldChangeGameStateFromStartToPlayAfterFirstInput() {
        assertTrue(game.getState() instanceof StartState);
        state.generateOutput(game, "");
        assertTrue(game.getState() instanceof PlayState);
    }
}
