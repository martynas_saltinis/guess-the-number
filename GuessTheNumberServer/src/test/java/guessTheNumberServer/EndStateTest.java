package guessTheNumberServer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EndStateTest {

    private Game game;
    private State state;

    @Before
    public void setupEnvironment() {
        game = new Game();
        state = new EndState();
        game.processInput("initial call");
    }

    @Test
    public void shouldAskIfYouWantToTryAgain() {
        assertEquals(state.generateOutput(game, ""), "Want to go again? (Y/N)");
    }

    @Test
    public void shouldRestartGameIfInputIsY() {
        game.processInput(String.format("%d", game.getAnswer() + 1));
        assertEquals(game.getGuessCount(), 1);
        game.processInput(String.format("%d", game.getAnswer()));
        state.generateOutput(game, "Y");
        assertEquals(game.getGuessCount(), 0);
        assertTrue(game.getState() instanceof PlayState);
    }

    @Test
    public void shouldQuitGameIfInputIsN() {
        game.processInput(String.format("%d", game.getAnswer()));
        assertEquals(game.processInput("n"), "Bye");
        assertEquals(game.processInput("N"), "Bye");
    }
}
