package guessTheNumberServer;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class ServerThreadIT {
    static ServerSocket socketServer;
    static Random random;
    static Socket socketClient;
    static PrintWriter outClient;
    static BufferedReader inClient;
    static BufferedReader stdInClient;

    @BeforeClass
    public static void setupEnvironment() {
            setupServer();
            setupClient();
            random = new Random();
    }

    public static void setupServer() {
        try {
            socketServer = new ServerSocket(5000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        new Thread()
        {
            public void run() {
                try {
                    Socket connection = socketServer.accept();
                    new ServerThread(connection).start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static void setupClient() {
        try {
            socketClient = new Socket("127.0.0.1", 5000);
            outClient = new PrintWriter(socketClient.getOutputStream(), true);
            inClient = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
            stdInClient = new BufferedReader(new InputStreamReader(System.in));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldPlayTwoMatchesAndShutDown() throws IOException {
        String fromServer;
        String fromUser;
        boolean firstMatch = true;
        while ((fromServer = inClient.readLine()) != null) {
            System.out.println("Server: " + fromServer);
            if (fromServer.equals("Bye")) {
                break;
            } else if(fromServer.contains("number")) {
                fromUser = generateNumberInput();
            } else if(firstMatch){
                fromUser = "y";
                firstMatch = false;
            } else {
                fromUser = "n";
            }
            if (fromUser != null) {
                System.out.println("Client: " + fromUser);
                outClient.println(fromUser);
            }
        }
    }

    public String generateNumberInput() {
        return Integer.toString((random.nextInt(10) + 1));
    }

    @AfterClass
    public static void shutdownServer() {
        try {
            socketServer.close();
            System.out.println("server shutdown");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
