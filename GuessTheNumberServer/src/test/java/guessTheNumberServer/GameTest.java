package guessTheNumberServer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GameTest {
    private Game game;

    @Before
    public void setupEnvironment() {
        game = new Game();
        game.processInput("initial call");
    }

    @Test
    public void shouldResetGuessCountAndSetPlayStateOnGameRestart() {
        game.restartGame();
        assertEquals(game.getGuessCount(), 0);
        assertTrue(game.getState() instanceof PlayState);
    }
}
