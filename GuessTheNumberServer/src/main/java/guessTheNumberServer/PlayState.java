package guessTheNumberServer;

public class PlayState implements State{

    @Override
    public String generateOutput(Game game, String input) {
        try {
            int inputNumber = Integer.parseInt(input);
            game.incrementGuessCount();
            if (inputNumber != game.getAnswer()) {
                if(inputNumber > game.getAnswer())
                    return "Incorrect. Try a smaller number.";
                else
                    return "Incorrect. Try a bigger number.";
            } else {
                game.setState(new EndState());
                return String.format("Correct! You won with %d guesses. Want to go again? (Y/N).", game.getGuessCount());
            }
        } catch (NumberFormatException e) {
            return "Please enter a number.";
        }
    }
}
