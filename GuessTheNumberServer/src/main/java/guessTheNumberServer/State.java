package guessTheNumberServer;

public interface State {
    String generateOutput(Game game, String input);
}
