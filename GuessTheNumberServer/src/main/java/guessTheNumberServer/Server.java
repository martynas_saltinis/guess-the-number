package guessTheNumberServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

class Server {
    private final static int SERVER_PORT = 5000;

    public static void main(String[] args) {
        try(
                ServerSocket serverSocket = new ServerSocket(SERVER_PORT)
        ) {
            System.out.println("Waiting for connections...");
            while(true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Received a connection.");
                new ServerThread(clientSocket).start();
            }
        } catch (IOException e) {
            System.err.println("Could not listen to port " + SERVER_PORT);
            System.err.println(e.getMessage());
        }
    }
}
