package guessTheNumberServer;

public class EndState implements State{

    @Override
    public String generateOutput(Game game, String input) {
        if(input.toLowerCase().equals("y")){
            game.restartGame();
            return String.format("Guess a number between %d and %d", game.getMinValue(), game.getMaxValue());
        } else if(input.toLowerCase().equals("n")) {
            return "Bye";
        } else {
            return "Want to go again? (Y/N)";
        }
    }
}
