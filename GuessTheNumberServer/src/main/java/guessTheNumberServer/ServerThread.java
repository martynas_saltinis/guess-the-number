package guessTheNumberServer;

import java.io.*;
import java.net.Socket;

class ServerThread extends Thread{
    private final Socket socket;

    public ServerThread(Socket socket) {
        super("ServerThread");
        this.socket = socket;
    }

    public void run() {
        try (
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))
        ) {
            System.out.println("Starting a game with thread: " + this.getId());
            String inputLine, outputLine;
            Game game = new Game();
            outputLine = game.processInput("");
            out.println(outputLine);
            while ((inputLine = in.readLine()) != null ) {
                outputLine = game.processInput(inputLine);
                out.println(outputLine);
                if (outputLine.equals("Bye"))
                    break;
            }
            System.out.println("Ending a game with thread: " + this.getId());
            socket.close();
        } catch (IOException e) {
            System.err.println(String.format("Thread %d %s", this.getId(), e.getMessage()));
        }
    }
}
