package guessTheNumberServer;

import java.util.Random;

class Game {
    private final int MIN_VALUE;
    private final int MAX_VALUE;
    private int answer;
    private int guessCount;
    private State state;

    public Game() {
        MIN_VALUE = 1;
        MAX_VALUE = 10;
        guessCount = 0;
        answer = getRandomNumberInIntervalInclusive(MAX_VALUE, MIN_VALUE);
        state = new StartState();
    }

    public String processInput(String input) {
        return state.generateOutput(this, input);
    }

    public void restartGame() {
        guessCount = 0;
        state = new PlayState();
        answer = getRandomNumberInIntervalInclusive(MAX_VALUE, MIN_VALUE);
    }

    private int getRandomNumberInIntervalInclusive(int max, int min) {
        Random random = new Random();
        return random.nextInt((MAX_VALUE - MIN_VALUE) + 1) + MIN_VALUE;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getMinValue() {
        return MIN_VALUE;
    }

    public int getMaxValue() {
        return MAX_VALUE;
    }

    public int getGuessCount() {
        return guessCount;
    }

    public void incrementGuessCount() {
        guessCount++;
    }

    public int getAnswer() {
        return answer;
    }
}
