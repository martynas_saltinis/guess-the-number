package guessTheNumberServer;

public class StartState implements State {

    @Override
    public String generateOutput(Game game, String input) {
        game.setState(new PlayState());
        return String.format("Guess a number between %d and %d", game.getMinValue(), game.getMaxValue());
    }
}
